This repository contains a set of scripts that can be used to create a
VM that matches the [OpenShift Installer on Libvirt with
KVM](https://github.com/openshift/installer/blob/master/docs/dev/libvirt-howto.md)
requirements.

Concretely, this means that we’ll create a cluster of KVM virtual
machines inside a virtual machine.

# Prerequisites

* [libvirt](https://libvirt.org/) up and running
* nested virtualisation activated:

```bash
$ cat /sys/module/kvm_intel/parameters/nested
Y
```

# Usage

Create the VM image with [mkosi](https://github.com/systemd/mkosi):

```bash
$ sudo mkosi
```

Move the VM image to the default libvirt storage pool:

```bash
$ sudo mv mkosi.output/openshift4-on-libvirt.qcow2 /var/lib/libvirt/images/
```

Start the VM:

```bash
$ virsh create domain.xml
```

The spawned virtual machine has a passwordless `auser` account and no
ssh server is running on it.

So, let’s connect to the virtual machine console and setup the
terminal:

```bash
$ echo $TERM $COLUMNS $LINES

xterm-256color 271 59

$ virsh console openshift4-on-libvirt

Connected to domain openshift4-on-libvirt
Escape character is ^]

openshift4-on-libvirt login: auser
[auser@openshift4-on-libvirt ~]$ export TERM=xterm-256color
[auser@openshift4-on-libvirt ~]$ stty cols 271
[auser@openshift4-on-libvirt ~]$ stty rows 59
```

Confirm that virtualisation extensions are available inside the
virtual machine (nested virtualisation activated):

```bash
[auser@openshift4-on-libvirt ~]$ lscpu
Architecture:        x86_64
[…]
Virtualization:      VT-x
Hypervisor vendor:   KVM
Virtualization type: full
[…]
```

OpenShift cluster can now be spawned:
```bash
[auser@openshift4-on-libvirt ~]$ openshift-install create cluster
? Platform  [Use arrows to move, type to filter]
> aws
  libvirt
? Platform  [Use arrows to move, type to filter]
  aws
> libvirt
? Platform libvirt
? Libvirt Connection URI [? for help] (qemu+tcp://192.168.122.1/system)
? Libvirt Connection URI qemu+tcp://192.168.122.1/system
? Base Domain [? for help] ?
…
```
